import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePaymentMethodDto } from './dto/create-paymentmethod.dto';
import { UpdatePaymentMethodDto } from './dto/update-paymentmethod.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PaymentMethod } from './entities/paymentmethod.entity';

@Injectable()
export class PaymentMethodService {
  constructor(
    @InjectRepository(PaymentMethod)
    private paymentMethodRepository: Repository<PaymentMethod>,
  ) {}

  create(createPaymentmethodDto: CreatePaymentMethodDto) {
    return this.paymentMethodRepository.save(createPaymentmethodDto);
  }

  findAll() {
    return this.paymentMethodRepository.find();
  }

  findOne(id: number) {
    return this.paymentMethodRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updatePaymentmethodDto: UpdatePaymentMethodDto) {
    try {
      const updatePaymentmethod = await this.paymentMethodRepository.save({
        id,
        ...updatePaymentmethodDto,
      });
      return updatePaymentmethod;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const paymentMethod = await this.paymentMethodRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedPaymentMethod =
        await this.paymentMethodRepository.remove(paymentMethod);
      return deletedPaymentMethod;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
