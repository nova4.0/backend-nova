import { Module } from '@nestjs/common';
import { PaymentMethodService } from './paymentmethod.service';
import { PaymentMethodController } from './paymentmethod.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PaymentMethod } from './entities/paymentmethod.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PaymentMethod])],
  controllers: [PaymentMethodController],
  providers: [PaymentMethodService],
})
export class PaymentMethodModule {}
