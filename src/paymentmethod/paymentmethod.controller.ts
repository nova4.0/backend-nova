import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { PaymentMethodService } from './paymentmethod.service';
import { CreatePaymentMethodDto } from './dto/create-paymentmethod.dto';
import { UpdatePaymentMethodDto } from './dto/update-paymentmethod.dto';

@Controller('paymentmethod')
export class PaymentMethodController {
  constructor(private readonly paymentmethodService: PaymentMethodService) {}

  @Post()
  create(@Body() createPaymentMethodDto: CreatePaymentMethodDto) {
    return this.paymentmethodService.create(createPaymentMethodDto);
  }

  @Get()
  findAll() {
    return this.paymentmethodService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.paymentmethodService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updatePaymentMethodDto: UpdatePaymentMethodDto,
  ) {
    return this.paymentmethodService.update(+id, updatePaymentMethodDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.paymentmethodService.remove(+id);
  }
}
