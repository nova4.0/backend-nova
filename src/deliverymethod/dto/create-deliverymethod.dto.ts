import { IsNotEmpty, IsString } from 'class-validator';

export class CreateDeliveryMethodDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  price: number;
}
