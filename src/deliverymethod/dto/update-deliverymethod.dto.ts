import { PartialType } from '@nestjs/mapped-types';
import { CreateDeliveryMethodDto } from './create-deliverymethod.dto';

export class UpdateDeliveryMethodDto extends PartialType(
  CreateDeliveryMethodDto,
) {}
