import { Module } from '@nestjs/common';
import { DeliveryMethodService } from './deliverymethod.service';
import { DeliveryMethodController } from './deliverymethod.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DeliveryMethod } from './entities/deliverymethod.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DeliveryMethod])],
  controllers: [DeliveryMethodController],
  providers: [DeliveryMethodService],
})
export class DeliveryMethodModule {}
