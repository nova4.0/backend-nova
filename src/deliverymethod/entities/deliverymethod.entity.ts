import { Delivery } from 'src/delivery/entities/delivery.entity';
import { Order } from 'src/order/entities/order.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class DeliveryMethod {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column({ default: ' ' })
  description: string;

  @Column({
    type: 'float',
    default: 0,
  })
  price: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => Order, (order) => order.deliveryMethod)
  order: Order[];
  @OneToMany(() => Delivery, (delivery) => delivery.deliveryMethod)
  delivery: Delivery;
}
