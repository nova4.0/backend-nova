import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { DeliveryMethodService } from './deliverymethod.service';
import { CreateDeliveryMethodDto } from './dto/create-deliverymethod.dto';
import { UpdateDeliveryMethodDto } from './dto/update-deliverymethod.dto';

@Controller('deliverymethod')
export class DeliveryMethodController {
  constructor(private readonly deliverymethodService: DeliveryMethodService) {}

  @Post()
  create(@Body() createDeliveryMethodDto: CreateDeliveryMethodDto) {
    return this.deliverymethodService.create(createDeliveryMethodDto);
  }

  @Get()
  findAll() {
    return this.deliverymethodService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.deliverymethodService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDeliveryMethodDto: UpdateDeliveryMethodDto,
  ) {
    return this.deliverymethodService.update(+id, updateDeliveryMethodDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.deliverymethodService.remove(+id);
  }
}
