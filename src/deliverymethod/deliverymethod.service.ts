import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateDeliveryMethodDto } from './dto/create-deliverymethod.dto';
import { UpdateDeliveryMethodDto } from './dto/update-deliverymethod.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DeliveryMethod } from './entities/deliverymethod.entity';

@Injectable()
export class DeliveryMethodService {
  constructor(
    @InjectRepository(DeliveryMethod)
    private deliveryMethodRepository: Repository<DeliveryMethod>,
  ) {}

  create(createDeliveryMethodDto: CreateDeliveryMethodDto) {
    return this.deliveryMethodRepository.save(createDeliveryMethodDto);
  }

  findAll() {
    return this.deliveryMethodRepository.find();
  }

  findOne(id: number) {
    return this.deliveryMethodRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateDeliveryMethodDto: UpdateDeliveryMethodDto) {
    try {
      const updateDeliveryMethod = await this.deliveryMethodRepository.save({
        id,
        ...updateDeliveryMethodDto,
      });
      return updateDeliveryMethod;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const deliveryMethod = await this.deliveryMethodRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedDeliveryMethod =
        await this.deliveryMethodRepository.remove(deliveryMethod);
      return deletedDeliveryMethod;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
