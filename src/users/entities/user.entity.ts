import { Customer } from 'src/customers/entities/customer.entity';
import { Delivery } from 'src/delivery/entities/delivery.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { Order } from 'src/order/entities/order.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;
  @Column({
    length: '64',
    default: '',
  })
  password: string;

  @Column({
    default: ' ',
  })
  address: string;

  @Column({
    length: '10',
    default: ' ',
  })
  telNumber: string;

  @Column({ length: '16', default: ' ' })
  cd_creditcardnumber: string;

  @Column({ default: ' ' })
  cd_expirationdate: string;

  @Column({
    length: '32',
    default: ' ',
  })
  cd_name: string;

  @Column({ length: '3', default: ' ' })
  cd_CVVnumber: string;

  @Column({ default: ' ' })
  cd_address: string;

  @OneToOne(() => Customer, (customer) => customer.user)
  @JoinColumn()
  customer: Customer;

  @OneToOne(() => Employee, (employee) => employee.user)
  @JoinColumn()
  employee: Employee;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => Order, (order) => order.user)
  order: Order;

  @OneToMany(() => Delivery, (delivery) => delivery.user)
  delivery: Delivery;
}
