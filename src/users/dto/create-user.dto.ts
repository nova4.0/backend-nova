import { IsNotEmpty, Length, Matches } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  @Matches(
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
  )
  password: string;

  @IsNotEmpty()
  telNumber: string;
  @IsNotEmpty()
  address: string;

  cd_creditcardnumber: string;

  cd_expirationdate: string;

  cd_name: string;

  cd_CVVnumber: string;

  cd_address: string;
}
