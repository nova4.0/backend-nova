import { Controller, Get, Request } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private authService: AuthService,
  ) {}

  @Get('profile')
  getProfile(@Request() req) {
    const userPayload = {
      user: req.user.user,
    };
    return userPayload;
  }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
