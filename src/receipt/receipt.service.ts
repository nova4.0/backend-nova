import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Receipt } from './entities/receipt.entity';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { Order } from 'src/order/entities/order.entity';
import { UpdateReceiptDto } from './dto/update-receipt.dto';

@Injectable()
export class ReceiptService {
  constructor(
    @InjectRepository(Receipt)
    private readonly receiptRepository: Repository<Receipt>,
    @InjectRepository(Order)
    private readonly orderRepository: Repository<Order>,
  ) {}

  async create(createReceiptDto: CreateReceiptDto): Promise<Receipt> {
    const { orderId, Date } = createReceiptDto;

    const order = await this.orderRepository.findOne({
      where: { id: orderId },
    });

    if (!order) {
      throw new NotFoundException(`Order with id ${orderId} not found`);
    }

    const receipt = new Receipt();
    receipt.order = order;
    receipt.Date = Date;

    return await this.receiptRepository.save(receipt);
  }

  async findAll(): Promise<Receipt[]> {
    return await this.receiptRepository.find();
  }

  async findById(id: number): Promise<Receipt | undefined> {
    return await this.receiptRepository.findOne({
      where: { id: id },
    });
  }

  async update(id: number, updateReceiptDto: UpdateReceiptDto) {
    try {
      const updateReceipt = await this.receiptRepository.save({
        id,
        ...updateReceiptDto,
      });
      return updateReceipt;
    } catch (e) {
      throw new NotFoundException();
    }
  }
  async remove(id: number): Promise<void> {
    await this.receiptRepository.delete(id);
  }
}
