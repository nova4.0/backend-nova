import { IsNotEmpty, Length } from 'class-validator';

export class CreateSupplierProductDto {
  @IsNotEmpty()
  @Length(3, 300)
  name: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  supplierId: number;

  image = 'No_Image_Available.jpg';
}
