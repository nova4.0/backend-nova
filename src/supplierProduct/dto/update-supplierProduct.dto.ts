import { PartialType } from '@nestjs/mapped-types';
import { CreateSupplierProductDto } from './create-supplierProduct.dto';

export class UpdateSupplierProductDto extends PartialType(
  CreateSupplierProductDto,
) {}
