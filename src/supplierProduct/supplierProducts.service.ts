import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Supplier } from 'src/supplier/entities/supplier.entity';
import { Repository } from 'typeorm';
import { CreateSupplierProductDto } from './dto/create-supplierProduct.dto';
import { UpdateSupplierProductDto } from './dto/update-supplierProduct.dto';
import { SupplierProduct } from './entities/supplierProduct.entity';

@Injectable()
export class SupplierProductsService {
  supplierproductsPartialType: any;
  constructor(
    @InjectRepository(SupplierProduct)
    private supplierproductsRepository: Repository<SupplierProduct>,
    @InjectRepository(Supplier)
    private suppliersRepository: Repository<Supplier>,
  ) {}

  async create(createSupplierProductDto: CreateSupplierProductDto) {
    const supplier = await this.suppliersRepository.findOneBy({
      id: createSupplierProductDto.supplierId,
    });
    const supplierproduct = new SupplierProduct();
    supplierproduct.name = createSupplierProductDto.name;
    supplierproduct.price = createSupplierProductDto.price;
    supplierproduct.image = createSupplierProductDto.image;
    supplierproduct.supplier = supplier;
    await this.supplierproductsRepository.save(supplierproduct);
    return this.supplierproductsRepository.findOne({
      where: { id: supplierproduct.id },
      relations: ['supplier'],
    });
  }

  findBySupplier(id: number) {
    return this.supplierproductsRepository.find({
      where: { supplierId: id },
    });
  }

  findAll() {
    return this.supplierproductsRepository.find({
      relations: ['supplier'],
    });
  }

  findOne(id: number) {
    return this.supplierproductsRepository.findOne({
      where: { id: id },
      relations: ['supplier'],
    });
  }

  async update(id: number, updateSupplierProductDto: UpdateSupplierProductDto) {
    console.log(updateSupplierProductDto);
    const supplierproduct = await this.supplierproductsRepository.findOneBy({
      id,
    });
    if (!supplierproduct) {
      throw new NotFoundException();
    }
    const updatedSupplierProduct = {
      ...supplierproduct,
      ...updateSupplierProductDto,
    };
    console.log(updateSupplierProductDto);
    return this.supplierproductsRepository.save(updatedSupplierProduct);
  }

  async remove(id: number) {
    const supplierproduct = await this.supplierproductsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedProduct =
        await this.supplierproductsRepository.remove(supplierproduct);
      return deletedProduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
