import { Module } from '@nestjs/common';
import { SupplierProductsService } from './supplierProducts.service';
import { SupplierProductsController } from './supplierProducts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SupplierProduct } from './entities/supplierProduct.entity';
import { Supplier } from 'src/supplier/entities/supplier.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SupplierProduct, Supplier])],
  controllers: [SupplierProductsController],
  providers: [SupplierProductsService],
})
export class SupplierProductsModule {}
