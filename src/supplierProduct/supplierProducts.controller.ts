import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  Res,
} from '@nestjs/common';
import { SupplierProductsService } from './supplierProducts.service';
import { CreateSupplierProductDto } from './dto/create-supplierProduct.dto';
import { UpdateSupplierProductDto } from './dto/update-supplierProduct.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';
import { extname } from 'path';
import { Response } from 'express';

@Controller('supplierproducts')
export class SupplierProductsController {
  constructor(
    private readonly supplierproductsService: SupplierProductsService,
  ) {}

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './supplierproduct_image',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createSupplierProductDto: CreateSupplierProductDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    createSupplierProductDto.image = file.filename;
    return this.supplierproductsService.create(createSupplierProductDto);
  }
  @Get('supplier/:id')
  findBySupplier(@Param('id') id: string) {
    return this.supplierproductsService.findBySupplier(+id);
  }
  // @Get()
  // findAll(@Query() query: { order?: string; orderBy?: string }) {
  //   return this.productsService.findAll({
  //     order: { [query.orderBy]: query.order },
  //   });
  // }
  @Get()
  findAll() {
    return this.supplierproductsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.supplierproductsService.findOne(+id);
  }

  @Get(':id/image')
  async getImage(@Param('id') id: string, @Res() res: Response) {
    const supplierproduct = await this.supplierproductsService.findOne(+id);
    res.sendFile(supplierproduct.image, { root: './supplierproduct_image' });
  }

  @Get(':image/:imageFile')
  async getImageByFileName(
    @Param('imageFile') imageFile: string,
    @Res() res: Response,
  ) {
    res.sendFile(imageFile, { root: './supplierproduct_image' });
  }

  @Patch(':id/image')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './supplierproduct_image',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  updateImage(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
  ) {
    return this.supplierproductsService.update(+id, { image: file.filename });
  }

  @Patch(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './supplierproduct_image',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(
    @Param('id') id: string,
    @Body() updateSupplierProductDto: UpdateSupplierProductDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      updateSupplierProductDto.image = file.filename;
    }
    return this.supplierproductsService.update(+id, updateSupplierProductDto);
  }
  //อัปเดทจำนวนสินค้า
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.supplierproductsService.remove(+id);
  }
}
