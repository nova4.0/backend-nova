import { PartialType } from '@nestjs/mapped-types';
import { CreateDeliverystatusDto } from './create-deliverystatus.dto';

export class UpdateDeliverystatusDto extends PartialType(
  CreateDeliverystatusDto,
) {}
