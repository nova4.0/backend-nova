import { IsNotEmpty } from 'class-validator';

export class CreateDeliverystatusDto {
  @IsNotEmpty()
  name: string;
}
