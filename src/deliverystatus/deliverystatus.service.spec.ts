import { Test, TestingModule } from '@nestjs/testing';
import { DeliverystatusService } from './deliverystatus.service';

describe('DeliverystatusService', () => {
  let service: DeliverystatusService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DeliverystatusService],
    }).compile();

    service = module.get<DeliverystatusService>(DeliverystatusService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
