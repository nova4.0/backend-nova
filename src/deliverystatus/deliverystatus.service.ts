import { Injectable, NotFoundException } from '@nestjs/common';

import { Repository } from 'typeorm';
import { Deliverystatus } from './entities/deliverystatus.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateDeliverystatusDto } from './dto/create-deliverystatus.dto';
import { UpdateDeliverystatusDto } from './dto/update-deliverystatus.dto';

@Injectable()
export class DeliverystatusService {
  constructor(
    @InjectRepository(Deliverystatus)
    private deliverystatusRepository: Repository<Deliverystatus>,
  ) {}

  create(createDeliverystatusDto: CreateDeliverystatusDto) {
    return this.deliverystatusRepository.save(createDeliverystatusDto);
  }

  findAll() {
    return this.deliverystatusRepository.find();
  }

  findOne(id: number) {
    return this.deliverystatusRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateDeliverystatusDto: UpdateDeliverystatusDto) {
    const deliverystatus = await this.deliverystatusRepository.findOneBy({
      id: id,
    });
    if (!deliverystatus) {
      throw new NotFoundException();
    }
    const updatedDeliverystatus = {
      ...deliverystatus,
      ...updateDeliverystatusDto,
    };
    return this.deliverystatusRepository.save(updatedDeliverystatus);
  }

  async remove(id: number) {
    const deliverystatus = await this.deliverystatusRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedDeliverystatus =
        await this.deliverystatusRepository.remove(deliverystatus);
      return deletedDeliverystatus;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
