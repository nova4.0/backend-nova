import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { DeliverystatusService } from './deliverystatus.service';

import { CreateDeliverystatusDto } from './dto/create-deliverystatus.dto';
import { UpdateDeliverystatusDto } from './dto/update-deliverystatus.dto';

@Controller('deliverystatus')
export class DeliverystatusController {
  constructor(private readonly deliverystatusService: DeliverystatusService) {}

  @Post()
  create(@Body() createDeliverystautsDto: CreateDeliverystatusDto) {
    return this.deliverystatusService.create(createDeliverystautsDto);
  }

  @Get()
  findAll() {
    return this.deliverystatusService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.deliverystatusService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDeliverystatusDto: UpdateDeliverystatusDto,
  ) {
    return this.deliverystatusService.update(+id, updateDeliverystatusDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.deliverystatusService.remove(+id);
  }
}
