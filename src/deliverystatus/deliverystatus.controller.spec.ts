import { Test, TestingModule } from '@nestjs/testing';
import { DeliverystatusController } from './deliverystatus.controller';
import { DeliverystatusService } from './deliverystatus.service';

describe('DeliverystatusController', () => {
  let controller: DeliverystatusController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DeliverystatusController],
      providers: [DeliverystatusService],
    }).compile();

    controller = module.get<DeliverystatusController>(DeliverystatusController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
