import { Module } from '@nestjs/common';
import { DeliverystatusService } from './deliverystatus.service';
import { DeliverystatusController } from './deliverystatus.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Deliverystatus } from './entities/deliverystatus.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Deliverystatus])],
  controllers: [DeliverystatusController],
  providers: [DeliverystatusService],
})
export class DeliverystatusModule {}
