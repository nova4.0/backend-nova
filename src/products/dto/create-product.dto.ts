import { IsNotEmpty, Length } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 300)
  name: string;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  minquantity: number;

  @IsNotEmpty()
  qtybefore: number;

  @IsNotEmpty()
  categoryId: number;

  image = 'No_Image_Available.jpg';
}
