import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from 'src/category/entities/category.entity';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  productsPartialType: any;
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Category)
    private categorysRepository: Repository<Category>,
  ) {}

  async create(createProductDto: CreateProductDto) {
    const category = await this.categorysRepository.findOneBy({
      id: createProductDto.categoryId,
    });
    const product = new Product();
    product.name = createProductDto.name;
    product.description = createProductDto.description;
    product.price = createProductDto.price;
    product.image = createProductDto.image;
    product.category = category;
    product.minquantity = createProductDto.minquantity;
    product.qtybefore = createProductDto.qtybefore;
    await this.productsRepository.save(product);
    return this.productsRepository.findOne({
      where: { id: product.id },
      relations: ['category'],
    });
  }

  findByCategory(id: number) {
    return this.productsRepository.find({
      where: { categoryId: id },
    });
  }

  findAll() {
    return this.productsRepository.find({
      relations: ['category'],
    });
  }

  findOne(id: number) {
    return this.productsRepository.findOne({
      where: { id: id },
      relations: ['category'],
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    console.log(updateProductDto);
    const product = await this.productsRepository.findOneBy({
      id,
    });
    if (!product) {
      throw new NotFoundException();
    }
    const updatedProduct = { ...product, ...updateProductDto };
    console.log(updateProductDto);
    return this.productsRepository.save(updatedProduct);
  }

  async remove(id: number) {
    const product = await this.productsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedProduct = await this.productsRepository.remove(product);
      return deletedProduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }
  async updateProductQuantity(id: number, quantity: number) {
    const product = await this.productsRepository.findOneBy({
      id,
    });
    if (!product) {
      throw new NotFoundException();
    }
    product.qtybefore = quantity;
    return this.productsRepository.save(product);
  }
}
