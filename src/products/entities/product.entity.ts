import { CartItem } from 'src/cart/entities/cartItem.entity';
import { Category } from 'src/category/entities/category.entity';
import { Delivery } from 'src/delivery/entities/delivery.entity';
import { OrderItem } from 'src/order/entities/orderItem.entity';
import { Stockproduct } from 'src/stockproduct/entities/stockproduct.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column()
  description: string;

  @Column({
    type: 'float',
  })
  price: number;

  @Column()
  minquantity: number;

  @Column()
  qtybefore: number;

  @Column({
    length: '128',
    default: 'no_img_avaliable.jpg',
  })
  image: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @ManyToOne(() => Category, (category) => category.product)
  category: Category;

  @OneToMany(() => CartItem, (cartItem) => cartItem.product)
  cartItems: CartItem[];

  @OneToMany(() => Stockproduct, (stockProduct) => stockProduct.product)
  stockProduct: Stockproduct;

  @Column()
  categoryId: number;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.product)
  orderItems: OrderItem[];

  @OneToMany(() => Delivery, (delivery) => delivery.product)
  delivery: Delivery;
}
