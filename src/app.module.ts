import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { CustomersModule } from './customers/customers.module';
import { EmployeeModule } from './employee/employee.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { User } from './users/entities/user.entity';
import { Customer } from './customers/entities/customer.entity';
import { Employee } from './employee/entities/employee.entity';
import { ProductsModule } from './products/products.module';
import { CategoryModule } from './category/category.module';

import { StockproductModule } from './stockproduct/stockproduct.module';
import { Stockproduct } from './stockproduct/entities/stockproduct.entity';
import { Category } from './category/entities/category.entity';
import { Product } from './products/entities/product.entity';
import { RolesModule } from './roles/roles.module';
import { Role } from './roles/entities/role.entity';
import { PromotionModule } from './promotion/promotion.module';
import { Promotion } from './promotion/entities/promotion.entity';
import { CartModule } from './cart/cart.module';
import { Cart } from './cart/entities/cart.entity';
import { CartItem } from './cart/entities/cartItem.entity';
import { OrderModule } from './order/order.module';
import { PaymentMethodModule } from './paymentmethod/paymentmethod.module';

import { PaymentMethod } from './paymentmethod/entities/paymentmethod.entity';
import { DeliveryMethodModule } from './deliverymethod/deliverymethod.module';
import { DeliveryMethod } from './deliverymethod/entities/deliverymethod.entity';
import { Order } from './order/entities/order.entity';
import { OrderItem } from './order/entities/orderItem.entity';

import { ReceiptModule } from './receipt/receipt.module';
import { Receipt } from './receipt/entities/receipt.entity';
import { DeliveryModule } from './delivery/delivery.module';

import { DeliverystatusModule } from './deliverystatus/deliverystatus.module';
import { Delivery } from './delivery/entities/delivery.entity';
import { Deliverystatus } from './deliverystatus/entities/deliverystatus.entity';
import { Supplier } from './supplier/entities/supplier.entity';
import { SupplierProduct } from './supplierProduct/entities/supplierProduct.entity';
import { SupplierProductsModule } from './supplierProduct/supplierProducts.module';
import { SupplierModule } from './supplier/supplier.module';
import { AuthModule } from './auth/auth.module';
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'nova.sqlite',
      synchronize: true,
      entities: [
        User,
        Customer,
        Employee,
        Product,
        Stockproduct,
        Category,
        Role,
        Promotion,
        Cart,
        CartItem,
        PaymentMethod,
        DeliveryMethod,
        Order,
        OrderItem,
        Receipt,
        Delivery,
        Deliverystatus,
        Supplier,
        SupplierProduct,
      ],
      logging: false,
      migrations: [],
      subscribers: [],
    }),

    UsersModule,
    CustomersModule,
    EmployeeModule,
    ProductsModule,
    CategoryModule,
    StockproductModule,
    RolesModule,
    PromotionModule,
    CartModule,
    OrderModule,
    PaymentMethodModule,
    DeliveryMethodModule,

    ReceiptModule,

    DeliveryModule,

    DeliverystatusModule,
    SupplierModule,
    SupplierProductsModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
