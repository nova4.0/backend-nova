import { Module } from '@nestjs/common';
import { DeliveryService } from './delivery.service';
import { DeliveryController } from './delivery.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Delivery } from './entities/delivery.entity';
import { Order } from 'src/order/entities/order.entity';
import { User } from 'src/users/entities/user.entity';
import { Deliverystatus } from 'src/deliverystatus/entities/deliverystatus.entity';
import { Product } from 'src/products/entities/product.entity';
import { DeliveryMethod } from 'src/deliverymethod/entities/deliverymethod.entity';
import { PaymentMethod } from 'src/paymentmethod/entities/paymentmethod.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Delivery,
      Order,
      User,
      Deliverystatus,
      Product,
      DeliveryMethod,
      PaymentMethod,
    ]),
  ],
  controllers: [DeliveryController],
  providers: [DeliveryService],
})
export class DeliveryModule {}
