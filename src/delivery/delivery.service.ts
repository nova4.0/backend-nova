import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Delivery } from './entities/delivery.entity';
import { UpdateDeliveryDto } from './dto/update-delivery.dto';
import { CreateDeliveryDto } from './dto/create-delivery.dto';
import { User } from 'src/users/entities/user.entity';
import { Order } from 'src/order/entities/order.entity';
import { Deliverystatus } from 'src/deliverystatus/entities/deliverystatus.entity';
import { Product } from 'src/products/entities/product.entity';
import { DeliveryMethod } from 'src/deliverymethod/entities/deliverymethod.entity';
import { PaymentMethod } from 'src/paymentmethod/entities/paymentmethod.entity';

@Injectable()
export class DeliveryService {
  constructor(
    @InjectRepository(Delivery)
    private readonly deliveryRepository: Repository<Delivery>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Order)
    private readonly orderRepository: Repository<Order>,
    @InjectRepository(Deliverystatus)
    private readonly deliverystatusRepository: Repository<Deliverystatus>,
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,

    @InjectRepository(DeliveryMethod)
    private readonly deliveryMethodRepository: Repository<DeliveryMethod>,
    @InjectRepository(PaymentMethod)
    private readonly paymentMethodRepository: Repository<PaymentMethod>,
  ) {}

  // async createDelivery(deliveryData: Partial<Delivery>): Promise<Delivery> {
  //   const delivery = this.deliveryRepository.create(deliveryData);
  //   return await this.deliveryRepository.save(delivery);
  // }
  async createDeliveryData(
    createDeliveryDto: CreateDeliveryDto,
  ): Promise<Delivery> {
    const order = await this.orderRepository.findOne({
      where: { id: createDeliveryDto.orderId },
    });
    const user = await this.userRepository.findOne({
      where: { id: createDeliveryDto.userId },
    });
    const product = await this.productRepository.findOne({
      where: { id: createDeliveryDto.productId },
    });
    const deliverystatus = await this.deliverystatusRepository.findOne({
      where: { id: createDeliveryDto.deliverystatusId },
    });

    const deliveryMethod = await this.deliveryMethodRepository.findOne({
      where: { id: createDeliveryDto.deliveryMethodId },
    });

    const paymentMethod = await this.paymentMethodRepository.findOne({
      where: { id: createDeliveryDto.paymentMethodId },
    });

    if (
      !order ||
      !user ||
      !product ||
      !deliverystatus ||
      !deliveryMethod ||
      !paymentMethod
    ) {
      throw new NotFoundException(
        'Order, User, Product, Deliverystatus, DeliveryMethod, or PaymentMethod not found.',
      );
    }

    // สร้าง Delivery entity จากข้อมูลที่ได้
    const delivery = this.deliveryRepository.create({
      telNumber: createDeliveryDto.telNumber,
      order,
      user,
      product,
      deliverystatus,
      deliveryMethod,
      paymentMethod,
      createdDate: new Date(),
    });

    // บันทึกข้อมูลลงในฐานข้อมูล
    return await this.deliveryRepository.save(delivery);
  }
  async createDelivery(createDeliveryDto: CreateDeliveryDto) {
    const order = await this.orderRepository.findOne({
      where: { id: createDeliveryDto.orderId },
    });
    const user = await this.userRepository.findOne({
      where: { id: createDeliveryDto.userId },
    });
    const deliverystatus = await this.deliverystatusRepository.findOne({
      where: { id: createDeliveryDto.deliverystatusId },
    });
    const delivery = new Delivery();
    delivery.order = order;
    delivery.user = user;
    delivery.deliverystatus = deliverystatus;
    await this.deliveryRepository.save(delivery);
  }

  async getDeliveryById(deliveryId: number): Promise<Delivery> {
    const delivery = await this.deliveryRepository.findOne({
      where: { id: deliveryId },
      relations: ['order', 'order.user'],
    });

    if (!delivery) {
      throw new NotFoundException(`Delivery with ID ${deliveryId} not found`);
    }

    return delivery;
  }
  // async getDeliveryById(deliveryId: number): Promise<Delivery> {
  //   const delivery = await this.deliveryRepository.findOne({
  //     where: { id: deliveryId },
  //     relations: ['order', 'order.user', 'order.deliverystatus'],
  //   });

  //   if (!delivery) {
  //     throw new NotFoundException(`Delivery with ID ${deliveryId} not found`);
  //   }

  //   return delivery;
  // }

  // async getAllDeliveries(): Promise<Delivery[]> {
  //   return await this.deliveryRepository.find({
  //     relations: [
  //       'order',
  //       'user',
  //       'deliverystatus',
  //       'paymentMethod',
  //       'deliveryMethod',
  //     ],
  //   });
  // }
  async getAllDeliveries(): Promise<Delivery[]> {
    return await this.deliveryRepository.find({
      relations: [
        'order',
        'user',
        'deliverystatus',
        'order.paymentMethod',
        'order.deliveryMethod',
        'order.items.product',
      ],
    });
  }

  // async update(id: number, updateDeliveryDto: UpdateDeliveryDto) {
  //   const order = await this.orderRepository.findOne({
  //     where: { id: updateDeliveryDto.orderId },
  //   });
  //   const delivery = await this.deliveryRepository.findOneBy({ id: id });
  //   if (!delivery) {
  //     throw new NotFoundException();
  //   }
  //   const updatedDelivery = { ...delivery, ...updateDeliveryDto };
  //   return this.deliveryRepository.save(updatedDelivery);
  // }
  async update(id: number, updateDeliveryDto: UpdateDeliveryDto) {
    const delivery = await this.deliveryRepository.findOneBy({ id: id });
    if (!delivery) {
      throw new NotFoundException();
    }
    const updatedDelivery = { ...delivery, ...updateDeliveryDto };
    return this.deliveryRepository.save(updatedDelivery);
  }

  async remove(id: number) {
    const delivery = await this.deliveryRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedDelivery = await this.deliveryRepository.remove(delivery);
      return deletedDelivery;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async getDeliveryDetails(deliveryId: number) {
    const delivery = await this.deliveryRepository.findOne({
      where: { id: deliveryId },
      relations: ['order', 'user', 'deliverystatus'],
    });

    if (!delivery) {
      throw new NotFoundException(`Delivery with ID ${deliveryId} not found`);
    }

    const order = delivery.order;
    const user = delivery.user;
    const deliverytatus = delivery.deliverystatus;

    const deliveryDetails = {
      orderId: order.id,
      orderTotalPrice: order.totalPrice,
      userName: user.name,
      deliveryStatus: deliverytatus.name,

      // deliveryDate: delivery.date,
      createdDate: delivery.createdDate,
    };

    return deliveryDetails;
  }
  async getOrderDetails(id: number): Promise<any> {
    const delivery = await this.deliveryRepository.findOne({
      where: { id: id },
      relations: [
        'order',
        'product',
        'deliveryMethod',
        'paymentMethod',
        'deliverystatus',
        'user',
      ],
    });

    const orderDetails = {
      orderId: delivery.order.id,
      productName: delivery.product.name,
      totalAmount: delivery.order.totalPrice,
      discount: delivery.order.discount,
      address: delivery.order.address,
      deliveryMethod: delivery.deliveryMethod.name,
      paymentMethod: delivery.paymentMethod.name,
      deliverystatus: delivery.deliverystatus.name,
      username: delivery.user.name,
      telNumber: delivery.telNumber,
    };

    return orderDetails;
  }
}
