import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Put,
  Delete,
  Patch,
} from '@nestjs/common';
import { DeliveryService } from './delivery.service';
import { Delivery } from './entities/delivery.entity';
import { UpdateDeliveryDto } from './dto/update-delivery.dto';
import { CreateDeliveryDto } from './dto/create-delivery.dto';
@Controller('delivery')
export class DeliveryController {
  constructor(private readonly deliveryService: DeliveryService) {}

  @Get(':id')
  async getDeliveryById(@Param('id') id: number): Promise<Delivery> {
    return this.deliveryService.getDeliveryById(id);
  }

  @Get()
  async getAllDeliveries(): Promise<Delivery[]> {
    return this.deliveryService.getAllDeliveries();
  }

  // @Post()
  // async createDelivery(
  //   @Body() deliveryData: Partial<Delivery>,
  // ): Promise<Delivery> {
  //   return this.deliveryService.createDelivery(deliveryData);
  // }
  @Post('create')
  async createDeliveryData(
    @Body() createDeliveryDto: CreateDeliveryDto,
  ): Promise<Delivery> {
    return await this.deliveryService.createDeliveryData(createDeliveryDto);
  }
  @Post()
  async createDelivery(@Body() createDeliveryDto: CreateDeliveryDto) {
    return this.deliveryService.createDelivery(createDeliveryDto);
  }

  @Put(':id')
  async updateDelivery(
    @Param('id') id: number,
    @Body() updateDeliveryDto: UpdateDeliveryDto,
  ): Promise<Delivery> {
    return this.deliveryService.update(id, updateDeliveryDto);
  }

  @Delete(':id')
  async removeDelivery(@Param('id') id: number): Promise<Delivery> {
    return this.deliveryService.remove(id);
  }

  @Get(':id/details')
  async getDeliveryDetails(@Param('id') deliveryId: number): Promise<any> {
    return await this.deliveryService.getDeliveryDetails(deliveryId);
  }
  @Get(':id/order-details')
  async getOrderDetails(@Param('id') deliveryId: number): Promise<any> {
    return await this.deliveryService.getOrderDetails(deliveryId);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDeliveryDto: UpdateDeliveryDto,
  ) {
    return this.deliveryService.update(+id, updateDeliveryDto);
  }
}
