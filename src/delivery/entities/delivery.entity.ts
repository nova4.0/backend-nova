import { DeliveryMethod } from 'src/deliverymethod/entities/deliverymethod.entity';
import { Deliverystatus } from 'src/deliverystatus/entities/deliverystatus.entity';
import { Order } from 'src/order/entities/order.entity';
import { PaymentMethod } from 'src/paymentmethod/entities/paymentmethod.entity';
import { Product } from 'src/products/entities/product.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  // Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  Column,
} from 'typeorm';

@Entity()
export class Delivery {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '10',
    default: '',
  })
  telNumber: string;

  @ManyToOne(() => Order, (order) => order.delivery)
  order: Order;

  @ManyToOne(() => User, (user) => user.delivery)
  user: User;

  @ManyToOne(() => Deliverystatus, (deliverystatus) => deliverystatus.delivery)
  deliverystatus: Deliverystatus;

  @ManyToOne(() => DeliveryMethod, (deliveryMethod) => deliveryMethod.delivery)
  deliveryMethod: DeliveryMethod;

  @ManyToOne(() => PaymentMethod, (paymentMethod) => paymentMethod.delivery)
  paymentMethod: PaymentMethod;

  @ManyToOne(() => Product, (product) => product.delivery)
  product: Product;
  @Column()
  orderId: number;
  @Column()
  userId: number;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
}
