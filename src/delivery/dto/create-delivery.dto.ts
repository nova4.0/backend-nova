export class CreateDeliveryDto {
  telNumber: string;

  orderId: number;

  userId: number;

  deliverystatusId: number;

  deliveryMethodId: number;

  paymentMethodId: number;

  productId: number;
}
