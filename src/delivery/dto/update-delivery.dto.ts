import { PartialType } from '@nestjs/mapped-types';
import { CreateDeliveryDto } from './create-delivery.dto';

export class UpdateDeliveryDto extends PartialType(CreateDeliveryDto) {
  telNumber: string;

  orderId: number;

  userId: number;

  deliverystatusId: number;

  deliveryMethodId: number;

  paymentMethodId: number;

  productId: number;
}
