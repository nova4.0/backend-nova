import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateStockproductDto } from './dto/create-stockproduct.dto';
import { UpdateStockproductDto } from './dto/update-stockproduct.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Stockproduct } from './entities/stockproduct.entity';
import { Repository } from 'typeorm';

@Injectable()
export class StockproductService {
  constructor(
    @InjectRepository(Stockproduct)
    private stockproductRepository: Repository<Stockproduct>,
  ) {}

  create(createStockProductDto: CreateStockproductDto) {
    return this.stockproductRepository.save(createStockProductDto);
  }

  findAll() {
    return this.stockproductRepository.find();
  }

  findOne(id: number) {
    return this.stockproductRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateStockproductDto: UpdateStockproductDto) {
    const stockProduct = await this.stockproductRepository.findOneBy({
      id: id,
    });
    if (!stockProduct) {
      throw new NotFoundException();
    }
    const updatedCustomer = { ...stockProduct, ...updateStockproductDto };
    return this.stockproductRepository.save(updatedCustomer);
  }

  async remove(id: number) {
    const stockproduct = await this.stockproductRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedStockproduct =
        await this.stockproductRepository.remove(stockproduct);
      return deletedStockproduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
