import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { StockproductService } from './stockproduct.service';
import { CreateStockproductDto } from './dto/create-stockproduct.dto';
import { UpdateStockproductDto } from './dto/update-stockproduct.dto';

@Controller('stockproduct')
export class StockproductController {
  constructor(private readonly stockproductService: StockproductService) {}

  @Post()
  create(@Body() createStockproductDto: CreateStockproductDto) {
    return this.stockproductService.create(createStockproductDto);
  }

  @Get()
  findAll() {
    return this.stockproductService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.stockproductService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateStockproductDto: UpdateStockproductDto,
  ) {
    return this.stockproductService.update(+id, updateStockproductDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.stockproductService.remove(+id);
  }
}
