import { Test, TestingModule } from '@nestjs/testing';
import { StockproductService } from './stockproduct.service';

describe('StockproductService', () => {
  let service: StockproductService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StockproductService],
    }).compile();

    service = module.get<StockproductService>(StockproductService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
