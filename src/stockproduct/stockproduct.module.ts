import { Module } from '@nestjs/common';
import { StockproductService } from './stockproduct.service';
import { StockproductController } from './stockproduct.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stockproduct } from './entities/stockproduct.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Stockproduct])],
  controllers: [StockproductController],
  providers: [StockproductService],
})
export class StockproductModule {}
