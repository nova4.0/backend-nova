import { Test, TestingModule } from '@nestjs/testing';
import { StockproductController } from './stockproduct.controller';
import { StockproductService } from './stockproduct.service';

describe('StockproductController', () => {
  let controller: StockproductController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StockproductController],
      providers: [StockproductService],
    }).compile();

    controller = module.get<StockproductController>(StockproductController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
