import { PartialType } from '@nestjs/mapped-types';
import { CreateStockproductDto } from './create-stockproduct.dto';

export class UpdateStockproductDto extends PartialType(CreateStockproductDto) {}
