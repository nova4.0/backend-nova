import { IsEmpty, IsNotEmpty } from 'class-validator';

export class CreateStockproductDto {
  @IsEmpty()
  qtyafter: number;

  @IsNotEmpty()
  productId: number;
}
