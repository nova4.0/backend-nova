import { Injectable, NotFoundException, Param } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Order } from './entities/order.entity';
import { OrderItem } from './entities/orderItem.entity';
import { Cart } from 'src/cart/entities/cart.entity';
import { PaymentMethod } from 'src/paymentmethod/entities/paymentmethod.entity';
import { DeliveryMethod } from 'src/deliverymethod/entities/deliverymethod.entity';
import { UpdateOrderDto } from './dto/update-order.dto';
import { CreateOrderDto } from './dto/create-order.dto';
import { Product } from 'src/products/entities/product.entity';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Cart)
    private readonly cartRepository: Repository<Cart>,
    @InjectRepository(Order)
    private readonly orderRepository: Repository<Order>,
    @InjectRepository(OrderItem)
    private readonly orderItemRepository: Repository<OrderItem>,
    @InjectRepository(PaymentMethod)
    private readonly paymentMethodRepository: Repository<PaymentMethod>,
    @InjectRepository(PaymentMethod)
    private readonly deliveryMethodRepository: Repository<DeliveryMethod>,
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    const cart = await this.cartRepository.findOne({
      where: { id: createOrderDto.cartId },
      relations: ['items', 'items.product'],
    });
    const paymentMethod = await this.paymentMethodRepository.findOne({
      where: { id: createOrderDto.paymentMethodId },
    });
    const deliveryMethod = await this.deliveryMethodRepository.findOne({
      where: { id: createOrderDto.deliveryMethodId },
    });
    const user = await this.userRepository.findOne({
      where: { id: createOrderDto.userId },
    });
    const order = this.orderRepository.create({
      discount: createOrderDto.discount,
      subtotal: createOrderDto.subtotal,
      totalPrice: createOrderDto.totalPrice,
      address: createOrderDto.address,
      slipImage: createOrderDto.slipImage,
      cart,
      paymentMethod,
      deliveryMethod,
      user,
    });

    await this.orderRepository.save(order);

    const orderItems = cart.items.map((item) => {
      const orderItem = new OrderItem();
      orderItem.order = order;
      orderItem.product = item.product;
      orderItem.quantity = item.quantity;
      orderItem.price = item.product.price;
      return orderItem;
    });

    await this.orderItemRepository.save(orderItems);

    return await this.orderRepository.findOne({
      where: { id: order.id },
      relations: ['items'],
    });
  }

  async getOrderItems(orderId: number): Promise<OrderItem[]> {
    const order = await this.orderRepository.findOne({
      where: { id: orderId },
      relations: ['items', 'items.product'],
    });

    if (!order) {
      throw new NotFoundException('Cart not found');
    }
    return order.items;
  }

  async createOrder(
    @Param('cartId') cartId: number,
    @Param('address') address: string,
    @Param('paymentMethodId') paymentMethodId: number,
    @Param('deliveryMethodId') deliveryMethodId: number,
    @Param('userId') userId: number,
    @Param('discount') discount: number,
    @Param('slipImage') slipImage: string,
  ): Promise<Order> {
    const cart = await this.cartRepository.findOne({
      where: { id: cartId },
      relations: ['items', 'items.product'],
    });

    if (!cart) {
      throw new Error('Cart not found');
    }

    const order = new Order();
    order.cart = cart;
    order.address = address;
    order.discount = discount;
    order.slipImage = slipImage;

    const subtotal = cart.items.reduce(
      (total, item) => total + item.product.price * item.quantity,
      0,
    );

    const totalPrice = subtotal - discount;

    order.subtotal = subtotal;
    order.totalPrice = totalPrice;
    const deliveryMethod = await this.deliveryMethodRepository.findOne({
      where: { id: deliveryMethodId },
    });

    if (!deliveryMethod) {
      throw new Error('Payment method not found');
    }

    order.deliveryMethod = deliveryMethod;

    const paymentMethod = await this.paymentMethodRepository.findOne({
      where: { id: paymentMethodId },
    });

    if (!paymentMethod) {
      throw new Error('Payment method not found');
    }

    order.paymentMethod = paymentMethod;

    await this.orderRepository.save(order);

    const user = await this.userRepository.findOne({
      where: { id: userId },
    });

    if (!user) {
      throw new Error('user not found');
    }

    order.user = user;

    await this.orderRepository.save(order);

    const orderItems = cart.items.map((item) => {
      const orderItem = new OrderItem();
      orderItem.order = order;
      orderItem.product = item.product;
      orderItem.quantity = item.quantity;
      orderItem.price = item.product.price;
      return orderItem;
    });

    await this.orderItemRepository.save(orderItems);

    return order;
  }

  async findAll(): Promise<Order[]> {
    return await this.orderRepository.find({
      relations: ['items', 'items.product', 'paymentMethod', 'user'],
    });
  }

  async findOne(id: number): Promise<Order> {
    const order = await this.orderRepository.findOne({
      where: { id },
      relations: ['items', 'items.product'],
    });

    if (!order) {
      throw new NotFoundException('Order not found');
    }

    return order;
  }

  async updateOrder(
    orderId: number,
    updateOrderDto: UpdateOrderDto,
  ): Promise<Order> {
    const order = await this.orderRepository.findOne({
      where: { id: orderId },
    });

    if (!order) {
      throw new NotFoundException('Order not found');
    }

    order.address = updateOrderDto.address || order.address;
    order.deliveryMethod = updateOrderDto.deliveryMethodId
      ? await this.deliveryMethodRepository.findOne({
          where: { id: updateOrderDto.deliveryMethodId },
        })
      : order.deliveryMethod;
    order.paymentMethod = updateOrderDto.paymentMethodId
      ? await this.paymentMethodRepository.findOne({
          where: { id: updateOrderDto.paymentMethodId },
        })
      : order.paymentMethod;

    await this.orderRepository.save(order);

    return order;
  }
  async removeOrder(orderId: number): Promise<void> {
    const order = await this.orderRepository.findOne({
      where: { id: orderId },
    });
    if (!order) {
      throw new NotFoundException('Cart item not found');
    }
    await this.orderRepository.remove(order);
  }
}
