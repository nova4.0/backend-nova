import { Module } from '@nestjs/common';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { Order } from './entities/order.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cart } from 'src/cart/entities/cart.entity';
import { DeliveryMethod } from 'src/deliverymethod/entities/deliverymethod.entity';
import { PaymentMethod } from 'src/paymentmethod/entities/paymentmethod.entity';
import { OrderItem } from './entities/orderItem.entity';
import { CartItem } from 'src/cart/entities/cartItem.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Product } from 'src/products/entities/product.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Order,
      OrderItem,
      Cart,
      DeliveryMethod,
      PaymentMethod,
      CartItem,
      Receipt,
      Product,
      User,
    ]),
  ],
  controllers: [OrderController],
  providers: [OrderService],
})
export class OrderModule {}
