/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Controller,
  Post,
  Body,
  Param,
  Delete,
  Get,
  Patch,
  UseInterceptors,
  UploadedFile,
  Res,
  BadRequestException,
} from '@nestjs/common';
import { OrderService } from './order.service';

import { Order } from './entities/order.entity';
import { UpdateOrderDto } from './dto/update-order.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { v4 as uuidv4 } from 'uuid';
import { CreateOrderDto } from './dto/create-order.dto';
import { Response } from 'express';
import { OrderItem } from './entities/orderItem.entity';

@Controller('orders')
export class OrderController {
  constructor(private readonly orderService: OrderService) {}
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './order_image',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  async create(
    @Body() createOrderDto: CreateOrderDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (!file) {
      throw new BadRequestException('No file uploaded');
    }
    if (file.size === 0) {
      createOrderDto.slipImage = 'no_img_available.jpg';
    } else {
      createOrderDto.slipImage = file.filename;
    }
    return this.orderService.create(createOrderDto);
  }

  // @Post()
  // @UseInterceptors(
  //   FileInterceptor('file', {
  //     storage: diskStorage({
  //       destination: './order_image',
  //       filename: (req, file, cb) => {
  //         const name = 'SlipOrder_' + req.body.orderId; // สร้างชื่อใหม่ตาม orderId
  //         return cb(null, name + extname(file.originalname));
  //       },
  //     }),
  //   }),
  // )
  // async create(
  //   @Body() createOrderDto: CreateOrderDto,
  //   @UploadedFile() file: Express.Multer.File,
  // ) {
  //   if (!file) {
  //     throw new BadRequestException('No file uploaded');
  //   }

  //   createOrderDto.slipImage = file.filename;
  //   return this.orderService.create(createOrderDto);
  // }

  @Post(
    ':cartId/:address/:paymentMethodId/:deliveryMethodId/user/:discount/:slipImage',
  )
  async createOrder(
    @Param('cartId') cartId: number,
    @Param('address') address: string,
    @Param('paymentMethodId') paymentMethodId: number,
    @Param('deliveryMethodId') deliveryMethodId: number,
    @Param('user') userId: number,
    @Param('discount') discount: number,
    @Param('slipImage') slipImage: string,
  ): Promise<Order> {
    return this.orderService.createOrder(
      cartId,
      address,
      paymentMethodId,
      deliveryMethodId,
      userId,
      discount,
      slipImage,
    );
  }

  @Get()
  async getOrders(): Promise<Order[]> {
    const orders = await this.orderService.findAll();
    return orders;
  }

  @Get(':orderId/items')
  async getOrderItems(@Param('orderId') orderId: number): Promise<OrderItem[]> {
    return await this.orderService.getOrderItems(orderId);
  }

  @Get(':id/image')
  async getImage(@Param('id') id: string, @Res() res: Response) {
    const order = await this.orderService.findOne(+id);
    res.sendFile(order.slipImage, { root: './order_image' });
  }

  @Get(':image/:imageFile')
  async getImageByFileName(
    @Param('imageFile') imageFile: string,
    @Res() res: Response,
  ) {
    res.sendFile(imageFile, { root: './order_image' });
  }

  // @Get(':id/image')
  // async getImage(@Param('id') orderId: number): Promise<{ slipImage: string }> {
  //   const order = await this.orderService.findOne(orderId);
  //   if (!order) {
  //     throw new Error('Order not found');
  //   }
  //   return { slipImage: order.slipImage };
  // }

  @Get(':id')
  async getOrderById(@Param('id') orderId: number): Promise<Order> {
    const order = await this.orderService.findOne(orderId);
    if (!order) {
      throw new Error('Order not found');
    }
    return order;
  }

  @Patch(':id')
  async updateOrder(
    @Param('id') id: number,
    @Body() updateOrderDto: UpdateOrderDto,
  ): Promise<Order> {
    return await this.orderService.updateOrder(id, updateOrderDto);
  }

  @Delete(':orderId')
  async removeOrder(@Param('orderId') orderId: number) {
    return this.orderService.removeOrder(orderId);
  }
}
