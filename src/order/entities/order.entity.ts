import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  DeleteDateColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { OrderItem } from './orderItem.entity';
import { Cart } from 'src/cart/entities/cart.entity';
import { PaymentMethod } from 'src/paymentmethod/entities/paymentmethod.entity';
import { DeliveryMethod } from 'src/deliverymethod/entities/deliverymethod.entity';

import { Receipt } from 'src/receipt/entities/receipt.entity';
import { User } from 'src/users/entities/user.entity';
import { Delivery } from 'src/delivery/entities/delivery.entity';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  discount: number;

  @Column()
  subtotal: number;

  @Column()
  totalPrice: number;

  @Column({
    default: ' ',
  })
  address: string;

  @Column({
    length: '128',
    default: 'no_img_avaliable.jpg',
  })
  slipImage: string;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  items: OrderItem[];

  @ManyToOne(() => Cart, (cart) => cart.order)
  cart: Cart;

  @ManyToOne(() => PaymentMethod, (paymentMethod) => paymentMethod.order)
  paymentMethod: PaymentMethod;

  @ManyToOne(() => DeliveryMethod, (deliveryMethod) => deliveryMethod.order)
  deliveryMethod: DeliveryMethod;

  @ManyToOne(() => User, (user) => user.order)
  user: User;

  @OneToMany(() => Receipt, (receipt) => receipt.order)
  receipt: Receipt;
  @OneToMany(() => Delivery, (delivery) => delivery.order)
  delivery: Delivery;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deleteDate: Date;
}
