import { PartialType } from '@nestjs/mapped-types';
import { CreateOrderDto } from './create-order.dto';
import { OrderItem } from '../entities/orderItem.entity';

export class UpdateOrderDto extends PartialType(CreateOrderDto) {
  // เพิ่มเติม properties ที่ต้องการแก้ไข
  address: string;
  paymentMethodId: number;
  deliveryMethodId: number;
  discount: number;

  // เพิ่มเติม properties สำหรับอัพเดท order items (ถ้ามี)
  orderItems: OrderItem[];
}
