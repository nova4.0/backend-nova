import { IsNotEmpty } from 'class-validator';

export class CreateOrderDto {
  cartId: number;

  userId: number;
  @IsNotEmpty()
  deliveryMethodId: number;

  @IsNotEmpty()
  paymentMethodId: number;

  @IsNotEmpty()
  discount: number;

  subtotal: number;

  totalPrice: number;

  @IsNotEmpty()
  address: string;

  slipImage = 'no_img_avaliable.jpg';

  items: CreateOrderItemDto[];
}

export class CreateOrderItemDto {
  @IsNotEmpty()
  productId: number;

  quantity: number;
}
