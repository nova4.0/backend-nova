import { IsNotEmpty, Length } from 'class-validator';

export class CreateSupplierDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;
}
