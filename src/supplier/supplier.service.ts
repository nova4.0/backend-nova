import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSupplierDto } from './dto/create-supplier.dto';
import { UpdateSupplierDto } from './dto/update-supplier.dto';
import { Supplier } from './entities/supplier.entity';

@Injectable()
export class SupplierService {
  constructor(
    @InjectRepository(Supplier)
    private supplierRepository: Repository<Supplier>,
  ) {}

  create(createSupplierDto: CreateSupplierDto) {
    return this.supplierRepository.save(createSupplierDto);
  }

  findAll() {
    return this.supplierRepository.find();
  }

  findOne(id: number) {
    return this.supplierRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateSupplierDto: UpdateSupplierDto) {
    try {
      const updateSupplier = await this.supplierRepository.save({
        id,
        ...updateSupplierDto,
      });
      return updateSupplier;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const supplierproduct = await this.supplierRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedSupplierProduct =
        await this.supplierRepository.remove(supplierproduct);
      return deletedSupplierProduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
