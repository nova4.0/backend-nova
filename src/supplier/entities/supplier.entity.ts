import { SupplierProduct } from 'src/supplierProduct/entities/supplierProduct.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Supplier {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(
    () => SupplierProduct,
    (supplierproduct) => supplierproduct.supplier,
  )
  SupplierProduct: SupplierProduct[];
  supplier: any;
  supplierproduct: any;
}
