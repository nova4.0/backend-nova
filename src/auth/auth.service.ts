import { Injectable, UnauthorizedException } from '@nestjs/common';

import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  constructor(private usersService: UsersService) {}

  // async validateUser(username: string, pass: string): Promise<any> {
  //   const user = await this.usersService.findOneByName(username);
  //   const isMatch = await bcrypt.compare(pass, user.password);
  //   if (user && isMatch) {
  //     const { password, ...result } = user;
  //     return result;
  //   }
  //   return null;
  // }
  // async login(user: User) {
  //   const payload = { username: user.name, sub: user.id };

  //   return {
  //     user,
  //     access_token: this.jwtService.sign(payload),
  //   };
  // }

  async signIn(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOneByName(username);
    if (user?.password !== pass) {
      throw new UnauthorizedException();
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { password, ...result } = user;
    // TODO: Generate a JWT and return it here
    // instead of the user object
    return result;
  }
}
