import { Controller, Post, Param, Get, Delete, Patch } from '@nestjs/common';
import { CartService } from './cart.service';
import { CartItem } from './entities/cartItem.entity';
import { Cart } from './entities/cart.entity';

@Controller('cart')
export class CartController {
  constructor(private readonly cartService: CartService) {}

  // สร้างตะกร้าใหม่สำหรับลูกค้า
  @Post(':customerId') //   {{server}}/cart/:customerId
  async createCart(@Param('customerId') customerId: number) {
    return await this.cartService.createCart(customerId); // เรียกใช้พื่อสร้างตะกร้าใหม่สำหรับลูกค้าที่ระบุตาม Id
  }

  // เพิ่มสินค้าในตะกร้า
  @Post(':cartId/add-item/:productId/:quantity') // {{server}}/cart/{cartId}/add-item/{productId}/{quantity}
  async addItemToCart(
    @Param('cartId') cartId: number,
    @Param('productId') productId: number,
    @Param('quantity') quantity: number,
  ): Promise<CartItem> {
    return await this.cartService.addItemToCart(cartId, productId, quantity); // เรียกใช้เพื่อเพิ่มสินค้าในตะกร้าที่ระบุตาม Id
  }

  // ดึงรายการสินค้าในตะกร้า
  @Get(':cartId/items') // {{server}}/cart/{cartId}/items
  async getCartItems(@Param('cartId') cartId: number): Promise<CartItem[]> {
    return await this.cartService.getCartItems(cartId); // เรียกใช้เพื่อดึงรายการสินค้าในตะกร้าที่ระบุตาม Id
  }

  // ดึงตะกร้าทั้งหมด
  @Get() // {{server}}/cart
  async getAllCarts(): Promise<Cart[]> {
    return await this.cartService.getAllCarts(); // เรียกใช้เพื่อดึงตะกร้าทั้งหมด
  }
  //อัปเดทจำนวนสินค้าในตะกร้า
  @Patch(':quantity/items/:itemId') //{{server}}/cart/items/:itemId
  async updateCartItemQuantity(
    @Param('itemId') itemId: number,
    @Param('quantity') quantity: number,
  ): Promise<CartItem> {
    return await this.cartService.updateCartItemQuantity(itemId, quantity);
  }

  // ลบสินค้าออกจากตะกร้า
  @Delete('items/:itemId') // {{server}}/cart/items/{itemId}
  async removeItemFromCart(@Param('itemId') itemId: number): Promise<void> {
    await this.cartService.removeItemFromCart(itemId); // เรียกใช้เพื่อลบสินค้าออกจากตะกร้าที่ระบุตาม Id
  }
}
