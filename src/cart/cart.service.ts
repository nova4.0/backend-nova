import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Cart } from './entities/cart.entity';
import { CartItem } from './entities/cartItem.entity';
import { Product } from 'src/products/entities/product.entity';

@Injectable()
export class CartService {
  constructor(
    @InjectRepository(Cart)
    private readonly cartRepository: Repository<Cart>,
    @InjectRepository(CartItem)
    private readonly cartItemRepository: Repository<CartItem>,
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
  ) {}

  async createCart(customerId: number): Promise<Cart> {
    const newCart = new Cart();
    newCart.customer = { id: customerId } as any; // Assuming there's a customer with customerId
    return await this.cartRepository.save(newCart);
  }

  async addItemToCart(
    Id: number,
    productId: number,
    quantity: number,
  ): Promise<CartItem> {
    console.log('cartId before findOne:', Id);
    const cart = await this.cartRepository.findOne({ where: { id: Id } });

    console.log('cartId after findOne:', Id);
    if (!cart) {
      throw new NotFoundException('Cart not found');
    }

    const product = { id: productId } as any; // Assuming there's a product with productId

    let cartItem = await this.cartItemRepository.findOne({
      where: { product, cart },
    });
    if (!cartItem) {
      cartItem = new CartItem();
      cartItem.product = product;
      cartItem.quantity = quantity;
      cartItem.cart = cart;
    } else {
      cartItem.quantity += quantity;
    }

    return await this.cartItemRepository.save(cartItem);
  }

  async getCartItems(cartId: number): Promise<CartItem[]> {
    const cart = await this.cartRepository.findOne({
      where: { id: cartId },
      relations: ['items', 'items.product'],
    });

    if (!cart) {
      throw new NotFoundException('Cart not found');
    }
    return cart.items;
  }
  async getAllCarts(): Promise<Cart[]> {
    return await this.cartRepository.find();
  }

  async getCartById(cartId: number): Promise<Cart | undefined> {
    return await this.cartRepository.findOne({ where: { id: cartId } });
  }
  async updateCartItemQuantity(
    cartItemId: number,
    quantity: number,
  ): Promise<CartItem> {
    const cartItem = await this.cartItemRepository.findOne({
      where: { id: cartItemId },
    });
    if (!cartItem) {
      throw new NotFoundException('Cart item not found');
    }

    cartItem.quantity = quantity;
    return await this.cartItemRepository.save(cartItem);
  }

  async removeItemFromCart(itemId: number): Promise<void> {
    const cartItem = await this.cartItemRepository.findOne({
      where: { id: itemId },
    });
    if (!cartItem) {
      throw new NotFoundException('Cart item not found');
    }
    await this.cartItemRepository.remove(cartItem);
  }
}
