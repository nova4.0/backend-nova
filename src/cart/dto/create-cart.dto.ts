import { IsNotEmpty, IsInt, Min } from 'class-validator';

export class CreateCartItemDto {
  @IsNotEmpty()
  @IsInt()
  productId: number;

  @IsNotEmpty()
  @IsInt()
  @Min(1)
  quantity: number; // จำนวนสินค้าที่ต้องการใส่ในตะกร้า
}

export class CreateCartDto {
  @IsNotEmpty()
  customerId: number;

  @IsNotEmpty()
  items: CreateCartItemDto[]; // รายการสินค้าในตะกร้า
}
