import { IsDateString, IsNotEmpty, Length } from 'class-validator';

export class CreatePromotionDto {
  @IsNotEmpty()
  @Length(3, 300)
  name: string;

  @IsDateString()
  @IsNotEmpty()
  startDate: Date;

  @IsDateString()
  @IsNotEmpty()
  endDate: Date;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  discount: number;
}
