import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Promotion } from './entities/promotion.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PromotionService {
  constructor(
    @InjectRepository(Promotion)
    private promotionsRepository: Repository<Promotion>,
  ) {}

  create(createPromotionDto: CreatePromotionDto) {
    return this.promotionsRepository.save(createPromotionDto);
  }

  findAll() {
    return this.promotionsRepository.find();
  }

  findOne(id: number) {
    return this.promotionsRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    const promotions = await this.promotionsRepository.findOneBy({ id: id });
    if (!promotions) {
      throw new NotFoundException();
    }
    const updatedPromotion = { ...promotions, ...updatePromotionDto };
    return this.promotionsRepository.save(updatedPromotion);
  }

  async remove(id: number) {
    const promotions = await this.promotionsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedPromotion =
        await this.promotionsRepository.remove(promotions);
      return deletedPromotion;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
