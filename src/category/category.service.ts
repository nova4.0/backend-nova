import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { Category } from './entities/category.entity';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category)
    private categoryRepository: Repository<Category>,
  ) {}

  create(createCategoryDto: CreateCategoryDto) {
    return this.categoryRepository.save(createCategoryDto);
  }

  findAll() {
    return this.categoryRepository.find();
  }

  findOne(id: number) {
    return this.categoryRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateCategoryDto: UpdateCategoryDto) {
    try {
      const updateCategory = await this.categoryRepository.save({
        id,
        ...updateCategoryDto,
      });
      return updateCategory;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const product = await this.categoryRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedProduct = await this.categoryRepository.remove(product);
      return deletedProduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
